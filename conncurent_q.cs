using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SingleTon
{
    class Program
    {
        public class DbConnection
        {

        }
        public class ConnPool
        {
            private const int MAX = 10;
            private System.Collections.Concurrent.ConcurrentQueue<DbConnection> m_connections;

            public DbConnection GetConnection()
            {
                DbConnection conn = null;
                while (!m_connections.TryDequeue(out conn))
                {
                    Thread.Sleep(100);
                }
                return conn;
            }

            public void ReturnConnection(DbConnection conn)
            {
                m_connections.Enqueue(conn);
            }

            // 1
            private ConnPool()
            {
                m_connections = new System.Collections.Concurrent.ConcurrentQueue<DbConnection>();
                for (int i = 0; i < MAX; i++)
                {
                    m_connections.Enqueue(new DbConnection());
                }
            }

            // 2
            private static ConnPool Instance;
            // 3
            private static object key = new object();
            // 4
            public static ConnPool GetInstance()
            {
                if (Instance == null)
                {
                    lock (key)
                    {
                        if (Instance == null)
                        {
                            Instance = new ConnPool();
                        }
                    }
                }
                return Instance;
            }
        }

        static void Main(string[] args)
        {
            DbConnection conn1 = ConnPool.GetInstance().GetConnection();
            ConnPool.GetInstance().ReturnConnection(conn1);
        }
    }
}
