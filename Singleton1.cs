using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleTon
{
    class Program
    {
        public class ClockSingleton
        {
            private static ClockSingleton Instance;
            private static object key = new object();
            public static ClockSingleton GetInstance()
            {
                if (Instance == null)
                {
                    lock (key)
                    {
                        if (Instance == null)
                        {
                            Instance = new ClockSingleton();
                        }
                    }
                }
                return Instance;
            }

            private ClockSingleton()
            {

            }

            public DateTime GetDateTimeByRegion()
            {
                return DateTime.Now; // should do something smart ...
            }
        }

        static void Main(string[] args)
        {
            // log4net
            // clock
            // db 

            // 1 instance of the class [static]
            // 2 acess will be using a static method
            // 3 every instance will be the same
            // 4 impossible to create more then 1 object
            // 5 thread-safe
            // 6 cannot change the singleton

            ClockSingleton.GetInstance().GetDateTimeByRegion();
            if (ClockSingleton.GetInstance() == ClockSingleton.GetInstance())
            {
                Console.WriteLine("equal");
            }
            else
            {
                Console.WriteLine("Not equal!");
            }

            // ClockSingleton.GetInstance() = null; // Error!
            ClockSingleton sing = ClockSingleton.GetInstance();
            sing = null; // will not change the singleton




        }
    }
}
